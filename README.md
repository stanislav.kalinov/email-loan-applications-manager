# Email Loan Applications Manager

***Team members:***

* Stanislav Kalinov
* Lilyana Shapkadjieva


***1. Purpose & scope***

The main aim of the project is to deliver a system, capable to facilitate tracking, monitoring and processing customer loan applications coming to TBI Bank via e-mail from merchant having signed official contracts for using TBI Bank credit facilities.

***2. Functional features***

**2.1. User authentication**

User authentication is performed by ASP.NET Identity authentication. Users cannot register in the application but must be created by another user with “Manager” rights.

**2.2. User Rights & Roles**

· Role “Operator” – contains only a single right – “Operator”

· Role “Manager” – contains only a single right – “Manager”

For the purpose of the project we consider roles and rights as one and the same thing.

**2.3. Loan Application format** 

No specific loan application format is defined.

**2.4. Email – format**

No specific email format is defined.

**2.5. Email / Application**

Each email contains a single loan application. There may be a cases where specific email does not contain any application, in such a case email is considered not valid.

**2.6. Email registration**

* Emails are pulled on regular intervals from Gmail API via ASP.Net Core timed background task with hosted service. After pulling each email is moved to another Gmail label created for the particular purpose.
* As soon as email is read from Gmail API it is recorded in the system DB. Only emails from registered by administrators merchants are saved in the DB.

**2.7. Email - application statuses**

* Not reviewed - default status – all incoming emails are classified as such.
* Invalid application – email which does not contain loan applications is marked by operator in such status. Only employees with “Manager” rights can return the email in “Not reviewed” status.
* New – All emails are reviewed and only the one contains valid loan application are set by employee with “New” status.
* Open – status marked for under processing – in order to set to “Open” status, the system requests Customer ID/EGN and customer phone information to be fulfilled by the bank employee.
* Closed

**2.8. Statuses flow**

Possible statuses transition depends on granted user/operator rights. They are as follows:

· **Operators** (users/employees with Operator right)

* Not reviewed -> Invalid Application
* Not reviewed -> New
* New -> Open
* Open -> Closed (allowed only if the application was set in Open status by the same operator)

· **Managers** (users/employees with Manager right)

* Not reviewed <-> Invalid Application
* Not reviewed <-> New
* New -> Open (regardless who put the application in Open status)
* Open-> New (regardless who put the application in Open status)
* Open - > Closed (regardless who put the application in Open status)
* Closed -> New


**2.9. Email/Application record**

Set of application record:

* Sender (email and name)
* Date received (date of email receiving within the mail server)
* Subject
* Body
* Count of attachments and total (sum) size of attachments (in kb/Mb)
* Customer unique ID / EGN – filled by bank employee / agent when change from New->Open status
* Customer contact phone – filled by bank employee / agent when change from New->Open status
* Date/Time of initial registration within the system
* Date/Time when the application was set in current status
* Date/Time when the application was set in terminal status (Closed or Invalid Application)

**2.10. Personal Data subject of GDPR**

Column level encryption implemented via Crypteron - a platform for securing backend applications in the public, private or hybrid cloud, certified partner with Microsoft Azure and Amazone Web Services, which provides a complex solution of data encryption, tamper protection, key management and audit trails at the application layer regardless of where data is actually stored.
A following fields contain customer private data and are encrypted within the DB:

* Sender
* Email body
* Customer unique ID / EGN

**2.11. Working screens**

**Email preview**

* Mail body preview
* All attachments – (file name, file size).

**List all emails**

The screen lists all emails.
Information for any email:

* DB ID
* Email DateTime received
* Sender
* Subject
* Status
* Icon indicating if there any attachments
* Email preview button – showing email preview onClick

Actions

* All mails: option to mark specific email as not valid
* Mails marked as “not valid”: option to remove the flag “not valid” (only for Managers role)

**List New applications/emails**

The screen lists “verified” emails waiting for processing.
Information for any email:

* DB ID
* Email DateTime received
* Sender
* Subject
* Time since is in current status (order by this field desc)
* Email preview button – showing email preview onClick

Actions

* Option to assign application for work (change status to Open)
* New fields for fulfillment - Customer ID/EGN and customer phone information
* In order assign application for work, Customer ID/EGN and customer phone shall be fulfilled and formally validated.

**List open applications/emails**

The screen lists only emails/application with Open status
Information for any email:

* DB ID
* Email DateTime received
* Sender
* Subject
* Time since is in current status (order by this field desc)
* Email preview button – showing email preview onClick
* User who put application in status “Open” status (only Manager role)

Actions

* Close application with Approve or Reject status
* Return application status to “New” (only Manager role)

**List closed applications screen**

The screen lists only emails/application with Closed status
Information for any email:

* DB ID
* Email DateTime received
* Sender
* Subject
* Status
* Email preview button – showing email preview onClick
* User who put application in status “closed” status (only Manager role)

Actions

* Return application status to “New” (only Manager role) 

**2.12. Audit log**

Audit log in Azure Log Analytics workspace is provided with following information set:

* TimeStamp
* Action performed (status change, etc.)
* User performed the action
* Last status
* New status

***3. Non-functional features***

**3.1. Architecture**

* Web Application (MVC) + Business Layer + Data Layer 

**3.2. Logging & operational**

Apache log4net centralized logging capability implemented with custom appenders for:
* File system logging
* Application console logging
* Azure Application Insights logging to receive comprehensive information about live application performance no matter on development or production environment  
* Azure Log Analytics Workspace custom audit log implemented with capabilities to query all logged information needed  
* Azure storage configured to store all application logs gathered   

**3.3. Technology Reference model**

* ASP.Net Core MVC 2.2
* HTML5, CSS3, JavaScript
* Database – Azure Database for PostgreSQL Server
* Entity Framework (EF) Core
* Logging – Apache log4net standard logging library

