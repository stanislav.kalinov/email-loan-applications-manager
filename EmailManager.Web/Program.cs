﻿using EmailManager.Web.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace EmailManager.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            EmailManagerDbContext.SeedUserAdmin(host);

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseApplicationInsights()
                .UseStartup<Startup>()
                .Build();
    }
}
