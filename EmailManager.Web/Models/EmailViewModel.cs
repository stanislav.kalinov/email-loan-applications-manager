﻿using EmailManager.Data.Models;
using System;
using System.Collections.Generic;

namespace EmailManager.Web.Models
{
    public class EmailViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public DateTime? ReceivedOn { get; set; }
        public string EmailBodyContent { get; set; }
        public string MerchantEmail { get; set; }
        public string EmailStatusType { get; set; }
        public int? EmailAttachmentsCount { get; set; }
        public ICollection<EmailAttachment> Attachments { get; set; }
        public string CurrentStatusTimeSpan { get; set; }
        public string UserName { get; set; }
        public LoanApplicationViewModel ApplicationViewModel { get; set; }
    }
}
