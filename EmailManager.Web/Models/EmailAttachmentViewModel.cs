﻿using EmailManager.Data.Models;

namespace EmailManager.Web.Models
{
    public class EmailAttachmentViewModel
    {
        public int Id { get; set; }
        public int? Size { get; set; }
        public string FileName { get; set; }
        public ClientEmail Email { get; set; }

    }
}
