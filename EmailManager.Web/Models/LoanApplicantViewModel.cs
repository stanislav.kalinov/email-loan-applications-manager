﻿using EmailManager.Data.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EmailManager.Web.Models
{
    public class LoanApplicantViewModel
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Please enter a valid name!")]
        [Display(Name = "Applicant name")]
        public string Name { get; set; }

        [Required]
        [RegularExpression("[0-9]+", ErrorMessage = "Personal number must contain up to 10 digits!")]
        [MaxLength(10)]
        [Display(Name = "Personal number")]
        public string PersonalNumber { get; set; }

        [Required]
        [RegularExpression(@"(\+)?(359|0)(|-| )8[789]\d{1}(|-| )\d{3}(|-| )\d{3}", ErrorMessage = "Please enter a valid phone number!")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public ICollection<LoanApplication> LoanApplications { get; set; }
    }
}
