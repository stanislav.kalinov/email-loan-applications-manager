﻿using System.Collections.Generic;

namespace EmailManager.Web.Models
{
    public class EmailsViewModel
    {
        public IReadOnlyCollection<EmailViewModel> Emails { get; set; }
        public EmailViewModel Emailview { get; set; }

    }
}

