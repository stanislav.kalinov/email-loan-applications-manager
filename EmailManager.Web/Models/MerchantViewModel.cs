﻿using System.ComponentModel.DataAnnotations;

namespace EmailManager.Web.Models
{
    public class MerchantViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Merchant name")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Please provide valid email address!")]
        [Display(Name = "Personal email")]
        public string PersonalEmail { get; set; }
    }
}

