﻿using EmailManager.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmailManager.Web.Models
{
    public class LoanApplicationViewModel
    {
        public int Id { get; set; }

        public DateTime? LoanDecisionSetOn { get; set; }

        public ApplicationUser User { get; set; }

        public LoanApplicantViewModel LoanApplicant { get; set; }

        public LoanDecision LoanDecision { get; set; }

        public EmailViewModel Email { get; set; }
    }
}
