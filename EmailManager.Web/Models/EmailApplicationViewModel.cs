﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailManager.Web.Models
{
    public class EmailApplicationViewModel
    {
        public EmailViewModel Email { get; set; }
        public LoanApplicationViewModel LoanApplication { get; set; }
    }
}
