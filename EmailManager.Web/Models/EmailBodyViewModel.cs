﻿using EmailManager.Data.Models;

namespace EmailManager.Web.Models
{
    public class EmailBodyViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public ClientEmail ClientEmail { get; set; }
    }
}
