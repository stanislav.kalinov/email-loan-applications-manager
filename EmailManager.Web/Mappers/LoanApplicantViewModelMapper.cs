﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;

namespace EmailManager.Web.Mappers
{
    public class LoanApplicantViewModelMapper : IViewModelMapper<LoanApplicant, LoanApplicantViewModel>
    {
        public LoanApplicantViewModel MapFrom(LoanApplicant entity)
        {
            var loanApplicantViewModel = new LoanApplicantViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                PersonalNumber = entity.PersonalNumber,
                PhoneNumber = entity.PhoneNumber,
                LoanApplications = entity.LoanApplications
            };

            return loanApplicantViewModel;
        }
    }
}
