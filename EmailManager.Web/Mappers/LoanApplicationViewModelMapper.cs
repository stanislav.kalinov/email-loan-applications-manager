﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using System;

namespace EmailManager.Web.Mappers
{
    public class LoanApplicationViewModelMapper : IViewModelMapper<LoanApplication, LoanApplicationViewModel>
    {
        private readonly IViewModelMapper<LoanApplicant, LoanApplicantViewModel> loanApplicantMapper;

        public LoanApplicationViewModelMapper(IViewModelMapper<LoanApplicant, LoanApplicantViewModel> loanApplicantMapper)
        {
            this.loanApplicantMapper = loanApplicantMapper ?? throw new ArgumentNullException(nameof(loanApplicantMapper));
        }

        public LoanApplicationViewModel MapFrom(LoanApplication entity)
        {
            var loanApplicationViewModel = new LoanApplicationViewModel
            {
                Id = entity.Id,
                LoanDecisionSetOn = entity.LoanDecisionSetOn,
                User = entity.User,
                LoanApplicant = this.loanApplicantMapper.MapFrom(entity.LoanApplicant),
                LoanDecision = entity.LoanDecision,
            };

            return loanApplicationViewModel;
        }
    }
}
