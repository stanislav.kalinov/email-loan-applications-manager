﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;

namespace EmailManager.Web.Mappers
{
    public class EmailBodyViewModelMapper : IViewModelMapper<EmailBody, EmailBodyViewModel>
    {
        public EmailBodyViewModel MapFrom(EmailBody entity)
        {
            var emailBodyViewModel = new EmailBodyViewModel
            {
                Id = entity.Id,
                Content = entity.Content,
                ClientEmail = entity.ClientEmail
            };

            return emailBodyViewModel;
        }
    }
}
