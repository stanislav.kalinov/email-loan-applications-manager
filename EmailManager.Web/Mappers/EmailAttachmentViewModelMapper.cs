﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;

namespace EmailManager.Web.Mappers
{
    public class EmailAttachmentViewModelMapper : IViewModelMapper<EmailAttachment, EmailAttachmentViewModel>
    {
        public EmailAttachmentViewModel MapFrom(EmailAttachment entity)
        {
            var emailattachmentViewModel = new EmailAttachmentViewModel
            {
                Id = entity.Id,
                Size = entity.Size,
                FileName = entity.FileName,
                Email = entity.Email

            };
            return emailattachmentViewModel;
        }
    }
}
