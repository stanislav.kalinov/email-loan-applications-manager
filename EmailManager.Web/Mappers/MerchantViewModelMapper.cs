﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;

namespace EmailManager.Web.Mappers
{
    public class MerchantViewModelMapper : IViewModelMapper<Merchant, MerchantViewModel>
    {
        public MerchantViewModel MapFrom(Merchant entity)
        {
            var merchantViewModel = new MerchantViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                PersonalEmail = entity.PersonalEmail,
            };

            return merchantViewModel;
        }
    }
}

