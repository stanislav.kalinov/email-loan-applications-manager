﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmailManager.Web.Mappers
{
    public class EmailsViewModelMapper : IViewModelMapper<IReadOnlyCollection<ClientEmail>, EmailsViewModel>
    {
        private readonly IViewModelMapper<ClientEmail, EmailViewModel> movieMapper;

        public EmailsViewModelMapper(IViewModelMapper<ClientEmail, EmailViewModel> movieMapper)
        {
            this.movieMapper = movieMapper ?? throw new ArgumentNullException(nameof(movieMapper));
        }

        public EmailsViewModel MapFrom(IReadOnlyCollection<ClientEmail> emails)
        {
            var emailsViewModel = new EmailsViewModel
            {
                Emails = emails.Select(this.movieMapper.MapFrom).ToList()
            };

            return emailsViewModel;
        }
    }
}