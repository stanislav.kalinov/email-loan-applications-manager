﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace EmailManager.Web.Mappers
{
    public static class MappersRegistrator
    {
        public static IServiceCollection AddCustomMappers(this IServiceCollection services)
        {
            services.AddScoped<IViewModelMapper<ClientEmail, EmailViewModel>, EmailViewModelMapper>();
            services.AddScoped<IViewModelMapper<EmailAttachment, EmailAttachmentViewModel>, EmailAttachmentViewModelMapper>();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<ClientEmail>, EmailsViewModel>, EmailsViewModelMapper>();
            services.AddScoped<IViewModelMapper<Merchant, MerchantViewModel>, MerchantViewModelMapper>();
            services.AddScoped<IViewModelMapper<LoanApplicant, LoanApplicantViewModel>, LoanApplicantViewModelMapper>();
            services.AddScoped<IViewModelMapper<LoanApplication, LoanApplicationViewModel>, LoanApplicationViewModelMapper>();

            return services;
        }
    }
}
