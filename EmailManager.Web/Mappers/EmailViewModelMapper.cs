﻿using EmailManager.Data.Models;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using EmailManager.Services.Enums;
using Crypteron.CipherObject;
using System;

namespace EmailManager.Web.Mappers
{
    public class EmailViewModelMapper : IViewModelMapper<ClientEmail, EmailViewModel>
    {

        public EmailViewModel MapFrom(ClientEmail entity)
        {
            Merchant merchant;
            try
            {
                merchant = entity.Merchant.Unseal();
            }
            catch (Exception ex)
            {
                merchant = entity.Merchant;
            }

            EmailBody emailBody;
            try
            {
                emailBody = entity.EmailBody.Unseal();
            }
            catch (Exception ex)
            {
                emailBody = entity.EmailBody;
            }

            var emailViewModel = new EmailViewModel
            {
                Id = entity.Id,
                Subject = entity.Subject,
                ReceivedOn = entity.ReceivedOn,
                EmailBodyContent = entity.EmailBody.Content,
                MerchantEmail = merchant.PersonalEmail,
                EmailStatusType = entity.EmailStatus.Type,
                EmailAttachmentsCount = entity.EmailAttachments.Count,
                Attachments = entity.EmailAttachments,
                CurrentStatusTimeSpan = DateTime.Now.Subtract(entity.CurrentStatusSetOn).ToString(@"dd\.hh\:mm\:ss"),
                UserName = entity.User?.UserName
            };

            return emailViewModel;
        }
    }
}



