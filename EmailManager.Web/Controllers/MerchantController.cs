﻿using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EmailManager.Web.Controllers
{
    public class MerchantController : Controller
    {
        private readonly IMerchantService merchantService;
        private readonly IViewModelMapper<Merchant, MerchantViewModel> merchantMapper;

        public MerchantController(IMerchantService merchantService, IViewModelMapper<Merchant, MerchantViewModel> merchantMapper)
        {
            this.merchantService = merchantService ?? throw new ArgumentNullException(nameof(merchantService));
            this.merchantMapper = merchantMapper ?? throw new ArgumentNullException(nameof(merchantMapper));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public IActionResult AddMerchant()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMerchant(MerchantViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var merchant = await this.merchantService.AddMerchantAsync(model.Name, model.PersonalEmail);

            return RedirectToAction("Index", "Home");
        }
    }
}