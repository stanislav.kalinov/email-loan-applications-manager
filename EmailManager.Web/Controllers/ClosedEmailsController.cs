﻿using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EmailManager.Web.Controllers
{
    public class ClosedEmailsController : Controller
    {

        private readonly IEmailService emailService;
        private readonly IViewModelMapper<ClientEmail, EmailViewModel> emailMapper;

        public ClosedEmailsController(IEmailService emailService, IViewModelMapper<ClientEmail, EmailViewModel> emailMapper)
        {
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
        }

        [HttpGet]

        public IActionResult ClosedApplication()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ListClosedEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.emailService.GetClosedEmailsCount();
            var emails = await this.emailService.ListClosedEmails(skip, pageSize, searchValue);

            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();

            var json = Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });

            json.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

            return json;
        }

        [HttpGet]
        public IActionResult Details(int Id)
        {
            var email = this.emailService.GetEmail(Id);
            var model = this.emailMapper.MapFrom(email);
            return PartialView("_ClosedApplication", model);
        }

    }
}
