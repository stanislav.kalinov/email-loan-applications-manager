﻿using Crypteron.CipherObject;
using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EmailManager.Web.Controllers
{
    public class LoanApplicationController : Controller
    {
        private readonly ILoanApplicationService loanApplicationService;
        private readonly ILoanApplicantService loanApplicantService;
        private readonly IViewModelMapper<LoanApplication, LoanApplicationViewModel> loanApplicationMapper;
        private readonly IViewModelMapper<ClientEmail, EmailViewModel> emailMapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailService emailService;

        public LoanApplicationController(ILoanApplicationService loanApplicationService, ILoanApplicantService loanApplicantService,
                                         IViewModelMapper<LoanApplication, LoanApplicationViewModel> loanApplicationMapper, 
                                         UserManager<ApplicationUser> userManager, IViewModelMapper<ClientEmail, EmailViewModel> emailMapper,
                                         IEmailService emailService)
        {
            this.loanApplicationService = loanApplicationService ?? throw new ArgumentNullException(nameof(loanApplicationService));
            this.loanApplicantService = loanApplicantService ?? throw new ArgumentNullException(nameof(loanApplicantService));
            this.loanApplicationMapper = loanApplicationMapper ?? throw new ArgumentNullException(nameof(loanApplicationMapper));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            _userManager = userManager;

        }

        [HttpGet]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AddLoanApplication()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddLoanApplication(LoanApplicationViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var loanApplicant = await this.loanApplicantService.AddLoanApplicantAsync(model.LoanApplicant.Name, model.LoanApplicant.PersonalNumber, 
                                                                                      model.LoanApplicant.PhoneNumber);

            var loanApplication = await this.loanApplicationService.AddLoanApplicationAsync(loanApplicant, model.Email.Id);

            return RedirectToAction("Index", "Home");
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);

        [HttpGet]
        public IActionResult Details(int id)
        {
            var email = this.emailService.GetEmail(id);
            var emailBodyContent = email.EmailBody.Content;
            var model = new EmailApplicationViewModel();
            //model.Email = this.emailMapper.MapFrom(email);
            //model.Email.EmailBodyContent = emailBodyContent.Unseal();

            var emailViewModel = this.emailMapper.MapFrom(email);
            var content = emailViewModel.EmailBodyContent;
            model.Email = emailViewModel;

            return PartialView("_AddLoanApplication", model);
        }

        [HttpPost]
        public IActionResult Details(EmailApplicationViewModel model)
        {
            var email = this.emailService.GetEmail(model.Email.Id);
            var loanApplication = email.LoanApplication;
            return PartialView("_AddLoanApplication", model);
        }


        [HttpPost]
        public async Task<IActionResult> ListNewEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.emailService.GetNewEmailsCount();
            var emails = await this.emailService.ListNewEmails(skip, pageSize, searchValue);


            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();

            var json = Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });

            json.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

            return json;
        }

    }
}