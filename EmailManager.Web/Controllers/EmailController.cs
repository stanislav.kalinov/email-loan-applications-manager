﻿using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Services.Enums;
using EmailManager.Web.Data;
using EmailManager.Web.Mappers.Contracts;
using EmailManager.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailManager.Web.Controllers
{
    public class EmailController : Controller
    {
        private readonly IEmailService emailService;
        private readonly IViewModelMapper<ClientEmail, EmailViewModel> emailMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<ClientEmail>, EmailsViewModel> emailsMapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmailController(IEmailService emailService, IViewModelMapper<ClientEmail, EmailViewModel> emailMapper, 
            IViewModelMapper<IReadOnlyCollection<ClientEmail>, EmailsViewModel> emailsMapper, 
            UserManager<ApplicationUser> userManager)
        {
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
            this.emailsMapper = emailsMapper ?? throw new ArgumentNullException(nameof(emailsMapper));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ListAllEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.emailService.GetEmailsCount();
            var emails = await this.emailService.ListAllEmails(skip, pageSize, searchValue);


            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();

            var json = Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });

            json.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

            return json;
        }

        [HttpGet]
        public IActionResult Details(int Id)
        {
            var email = this.emailService.GetEmail(Id);
            var model = this.emailMapper.MapFrom(email);
            return PartialView("_DetailsPartial", model);
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);

        [HttpPost]
        public async Task<IActionResult> ChangeStatusToNotReviewedAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = await this.emailService.ChangeStatusToNotReviewedAsync(emailId, currentUser);

            return Ok("Ok!");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatusToNotValidAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = await this.emailService.ChangeStatusToNotValidAsync(emailId, currentUser);

            return Ok("Ok!");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatusToNewAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = await this.emailService.ChangeStatusToNewAsync(emailId, currentUser);

            return Ok("Ok!");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatusToOpenAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = await this.emailService.ChangeStatusToOpenAsync(emailId, currentUser);

            return Ok("Ok!");
        }

        [HttpPost]
        public async Task<IActionResult> LoanApprovedAsync(int emailid)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = await this.emailService.LoanApprovedAsync(emailid, currentUser);

            return Ok("Ok!");
        }

        [HttpPost]
        public async Task<IActionResult> LoanRejectedAsync(int emailid)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = await this.emailService.LoanRejectedAsync(emailid, currentUser);

            return Ok("Ok!");
        }
    }
}


