﻿using EmailManager.Data.DTO_sAndOptions;
using EmailManager.Services;
using EmailManager.Services.Contracts;
using EmailManager.Web.Data;
using EmailManager.Web.Mappers;
using EmailManager.Web.Middlewares;
using EmailManager.Web.Models;
using EmailManager.Web.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;

namespace EmailManager.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EmailManagerDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<EmailManagerDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IMerchantService, MerchantService>();
            services.AddScoped<IEmailAttachmentService, EmailAttachmentService>();
            services.AddScoped<ILoanApplicationService, LoanApplicationService>();
            services.AddScoped<ILoanApplicantService, LoanApplicantService>();
            services.AddScoped<IChangePasswordService, ChangePasswordService>();

            services.AddHostedService<GmailHostedService>();

            services.AddHttpClient();

            services.AddScoped<IGmailAuthService, GmailAuthService>();
            services.AddScoped<IGmailApiService, GmailApiService>();

            services.Configure<GmailApiUrlOptions>(this.Configuration.GetSection("GmailApiUrlOptions"));
            services.Configure<GoogleAuthOptions>(this.Configuration.GetSection("GoogleAuthOptions"));

            services.AddCustomMappers();

            services.AddMvc().AddJsonOptions
               (options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddResponseCaching();

            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_2);

            Crypteron.CrypteronConfig.Config.MyCrypteronAccount.AppSecret = Configuration["CrypteronConfig:MyCrypteronAccount:AppSecret"];

            services.AddApplicationInsightsTelemetry();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseMiddleware<EmailManagerExceptionsMiddleware>();
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMiddleware<EmailManagerResponseCachingMiddleware>();

            loggerFactory.AddLog4Net();

            loggerFactory.AddApplicationInsights(app.ApplicationServices, LogLevel.Debug);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Gmail}/{action=StartProgram}/{id?}");
            });
        }
    }
}
