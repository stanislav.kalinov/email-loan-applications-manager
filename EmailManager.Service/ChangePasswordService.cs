﻿using EmailManager.Services.Contracts;
using EmailManager.Web.Data;
using EmailManager.Web.Models;
using System;

namespace EmailManager.Services
{
    public class ChangePasswordService : IChangePasswordService
    {
        private readonly EmailManagerDbContext context;

        public ChangePasswordService(EmailManagerDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async void ChangePasswordConfirm(ApplicationUser user)
        {
            user.ChangePassword = true;
            await this.context.SaveChangesAsync();
        }
    }
}

