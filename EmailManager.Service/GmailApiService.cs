﻿using Crypteron.CipherObject;
using EmailManager.Data.DTO_sAndOptions;
using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Services.Enums;
using EmailManager.Web.Data;
using log4net;
using log4net.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class GmailApiService : IGmailApiService
    {
        const string CUSTOM_LABEL_NAME = "Label_9051538194651154262";

        private static ILoggerRepository REPOSITORY = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
        private static readonly ILog log2 = LogManager.GetLogger(REPOSITORY.Name, "GmailApiServiceLogger");

        private readonly IHttpClientFactory factory;
        private readonly GmailApiUrlOptions gmailUrlOptions;
        private readonly IGmailAuthService gmailAuthService;
        private readonly EmailManagerDbContext context;
        private readonly IMerchantService merchantService;
        private readonly IEmailAttachmentService emailAttachmentService;

        public GmailApiService(IHttpClientFactory factory, IOptions<GmailApiUrlOptions> gmailOptions, IGmailAuthService gmailAuthService, 
            EmailManagerDbContext context, IMerchantService merchantService, IEmailAttachmentService emailAttachmentService)
        {
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this.gmailUrlOptions = gmailOptions?.Value ?? throw new ArgumentNullException(nameof(gmailOptions));
            this.gmailAuthService = gmailAuthService ?? throw new ArgumentNullException(nameof(gmailAuthService));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.merchantService = merchantService ?? throw new ArgumentNullException(nameof(merchantService));
            this.emailAttachmentService = emailAttachmentService ?? throw new ArgumentNullException(nameof(emailAttachmentService));
        }

        public async Task SaveEmailsToDbAsync(string accessToken)
        {
            var messageList = await this.SendRequestAsync<GmailMessageListObject>(this.gmailUrlOptions.Messages, accessToken);

            if (messageList.Messages == null)
            {
                return;
            }

            for (int i = messageList.Messages.Length - 1; i >= 0; i--)
            {
                var mailboxEmailId = messageList.Messages[i].Id;

                var message = await this.SendRequestAsync<GmailMessageObject>(this.gmailUrlOptions.Message + mailboxEmailId, accessToken);

                await this.SendModifyRequestAsync(this.gmailUrlOptions.Message + mailboxEmailId + "/modify", accessToken);
                //await this.SendTrashRequestAsync(this.gmailUrlOptions.Message + mailboxEmailId + "/trash", accessToken);

                string from = "";
                string dateHeader = "";
                string subject = "";
                string body = "";
                string senderName = "";
                string senderEmail = "";

                IList<MessagePartHeader> parts = message.Payload?.Headers;

                foreach (var part in parts)
                {
                    if (part.Name == "From")
                    {
                        from = part.Value;

                        var senderEmailMatch = System.Text.RegularExpressions.Regex.Match(from, "<(.+)>");
                        senderName = from.Substring(0, senderEmailMatch.Index);
                        senderEmail = senderEmailMatch.Groups[1].Value;
                    }
                    else if (part.Name == "Date")
                    {
                        dateHeader = part.Value;
                    }
                    else if (part.Name == "Subject")
                    {
                        subject = part.Value;
                    }

                    if (dateHeader != "" && from != "")
                    {
                        if (message.Payload.Parts == null && message.Payload.Body != null)
                        {
                            body = DecodeBase64String(message.Payload.Body.Data);
                            var result = body.IndexOf('<');
                            body = body.Substring(0, result);
                        }
                        else
                        {
                            body = GetNestedBodyParts(message.Payload.Parts, "");
                            var result = body.IndexOf('<');
                            body = body.Substring(0, result);
                        }
                    }
                }

                var merchant = await this.context.Merchants.FirstOrDefaultAsync(m => m.PersonalEmail == senderEmail);

                if (merchant == null)
                {
                    continue;
                }

                var timestamp = message.InternalDate;
                var receivedOn = DateTimeOffset.FromUnixTimeMilliseconds(timestamp).DateTime.ToUniversalTime().AddHours(3);

                var emailBody = new EmailBody()
                {
                    Content = body,
                };

                emailBody.Seal();

                await this.context.EmailBodies.AddAsync(emailBody);

                var emailStatus = await this.context.EmailStatuses.FirstOrDefaultAsync(es => es.Type == ((EmailStatusType)1).ToString());

                if (emailStatus == null)
                {
                    emailStatus = new EmailStatus()
                    {
                        Type = ((EmailStatusType)1).ToString(),
                    };

                    await this.context.EmailStatuses.AddAsync(emailStatus);
                }

                var email = new ClientEmail()
                {
                    MailboxEmailId = mailboxEmailId,
                    Timestamp = timestamp,
                    ReceivedOn = receivedOn,
                    Subject = subject,
                    Merchant = merchant,
                    EmailBody = emailBody,
                    EmailStatus = emailStatus,
                    CurrentStatusSetOn = DateTime.Now
                };

                await this.context.Emails.AddAsync(email);

                IList<MessagePart> attachments = message.Payload?.Parts;

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                    {
                        if (!String.IsNullOrEmpty(attachment.FileName))
                        {
                            var emailAttachment = await this.emailAttachmentService.AddEmailAttachmentAsync(attachment.FileName, attachment.Body.Size);
                            emailAttachment.Email = email;
                        }
                    }
                }

                await this.context.SaveChangesAsync();

                log2.Info($"Email with Id: {email.MailboxEmailId} have been saved");
            }
        }

        private async Task<TObject> SendRequestAsync<TObject>(string url, string accessToken)
        {
            var client = this.factory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);

            var response = await client.GetAsync(this.gmailUrlOptions.Base + url + this.gmailUrlOptions.InboxMessages);

            var result = JsonConvert.DeserializeObject<TObject>(await response.Content.ReadAsStringAsync());

            return result;
        }

        private async Task SendModifyRequestAsync(string url, string accessToken)
        {
            var client = this.factory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            var modifyMessageLabels = new ModifyMessageRequestContent()
            {
                AddLabels = new string[] { CUSTOM_LABEL_NAME },
                RemoveLabels = new string[] { "INBOX" }
            };

            var content = JsonConvert.SerializeObject(modifyMessageLabels);

            var httpContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(this.gmailUrlOptions.Base + url, httpContent);
        }

        private async Task SendTrashRequestAsync(string url, string accessToken)
        {
            var client = this.factory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);

            var httpContent = new StringContent("");

            var response = await client.PostAsync(this.gmailUrlOptions.Base + url, httpContent);
        }

        private static string DecodeBase64String(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "<strong>Message body was not returned</strong>";

            string InputStr = input.Replace("-", "+").Replace("_", "/");
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(InputStr));
        }

        private static string GetNestedBodyParts(IList<MessagePart> part, string curr)
        {
            string str = curr;
            if (part == null)
            {
                return str;
            }
            else
            {
                foreach (var parts in part)
                {
                    if (parts.Parts == null)
                    {
                        if (parts.Body != null && parts.Body.Data != null)
                        {
                            var ts = DecodeBase64String(parts.Body.Data);
                            str += ts;
                        }
                    }
                    else
                    {
                        return GetNestedBodyParts(parts.Parts, str);
                    }
                }

                return str;
            }
        }
    }
}
