﻿using Crypteron.CipherObject;
using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Services.Exceptions;
using EmailManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class LoanApplicantService : ILoanApplicantService
    {
        private readonly EmailManagerDbContext context;

        public LoanApplicantService(EmailManagerDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<LoanApplicant> AddLoanApplicantAsync(string name, string personalNumber, string phoneNumber)
        {
            var loanApplicant = await this.context.LoanApplicants.FirstOrDefaultAsync(m => m.PersonalNumber == personalNumber);

            if (loanApplicant != null)
            {
                return loanApplicant;
            }

            loanApplicant = new LoanApplicant()
            {
                Name = name,
                PersonalNumber = personalNumber,
                PhoneNumber = phoneNumber
            };

            loanApplicant.Seal();

            await this.context.LoanApplicants.AddAsync(loanApplicant);
            await this.context.SaveChangesAsync();

            return loanApplicant;
        }
    }
}
