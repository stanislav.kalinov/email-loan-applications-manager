﻿namespace EmailManager.Services.Enums
{
    public enum LoanApplicationDecision
    {
        Approved = 1,
        Rejected = 2,
    }
}
