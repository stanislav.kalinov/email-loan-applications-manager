﻿namespace EmailManager.Services.Enums
{
    public enum EmailStatusType
    {
       NotReviewed = 1,
       NotValid = 2,
       New = 3,
       Open = 4,
       Closed = 5
    }
}
