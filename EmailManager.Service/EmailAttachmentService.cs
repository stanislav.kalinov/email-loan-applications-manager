﻿using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Web.Data;
using System;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class EmailAttachmentService : IEmailAttachmentService
    {
        private readonly EmailManagerDbContext context;

        public EmailAttachmentService(EmailManagerDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<EmailAttachment> AddEmailAttachmentAsync(string fileName, int? size)
        {
            var attachment = new EmailAttachment()
            {
                FileName = fileName,
                Size = size
            };

            await this.context.EmailAttachments.AddAsync(attachment);
            await this.context.SaveChangesAsync();

            return attachment;
        }
    }
}
