﻿using EmailManager.Services.Exceptions.Contracts;
using System.Net;

namespace EmailManager.Services.Exceptions
{
    public class MovieNotFoundException : BaseEmailManagerException
    {
        public MovieNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
