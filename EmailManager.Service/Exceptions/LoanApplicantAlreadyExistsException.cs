﻿using EmailManager.Services.Exceptions.Contracts;
using System.Net;

namespace EmailManager.Services.Exceptions
{
    public class LoanApplicantAlreadyExistsException : BaseEmailManagerException
    {
        public LoanApplicantAlreadyExistsException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
