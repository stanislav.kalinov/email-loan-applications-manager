﻿using EmailManager.Services.Exceptions.Contracts;
using System.Net;

namespace EmailManager.Services.Exceptions
{
    public class MerchantAlreadyExistsException : BaseEmailManagerException
    {
        public MerchantAlreadyExistsException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
