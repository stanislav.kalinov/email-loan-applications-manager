﻿using System;

namespace EmailManager.Services.Exceptions.Contracts
{
    public class BaseEmailManagerException : Exception
    {
        private int code;

        public int Code { get; }

        public BaseEmailManagerException(string message, int code) : base(message)
        {
            this.code = code;
        }
    }
}
