﻿using EmailManager.Services.Exceptions.Contracts;
using System.Net;

namespace EmailManager.Services.Exceptions
{
    public class GoogleResponseException : BaseEmailManagerException
    {
        public GoogleResponseException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
