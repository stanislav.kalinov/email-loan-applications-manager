﻿using EmailManager.Services.Contracts;
using log4net;
using log4net.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace EmailManager.Services
{
    public class GmailHostedService : IHostedService
    {
        private static ILoggerRepository REPOSITORY = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
        private static readonly ILog log3 = LogManager.GetLogger(REPOSITORY.Name, "GmailHostedServiceLogger");

        private readonly IServiceProvider serviceProvider;
        private Timer timer;

        public GmailHostedService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            log3.Info("Timed Background Service is starting.");

            this.timer = new Timer(GetNewEmailsFromGmail, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        private async void GetNewEmailsFromGmail(object state)
        {
            log3.Info("Scope started.");

            using (var scope = this.serviceProvider.CreateScope())
            {
                var gmailAuthService = scope.ServiceProvider.GetRequiredService<IGmailAuthService>();

                var credentials = await gmailAuthService.CheckForTokensAsync();

                if (credentials == null)
                {
                    return;
                }
                else
                {
                    var accessToken = await gmailAuthService.GetAccessToken(credentials);

                    var gmailApiService = scope.ServiceProvider.GetRequiredService<IGmailApiService>();

                    await gmailApiService.SaveEmailsToDbAsync(accessToken);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            log3.Info("Timed Background Service is stopping.");

            this.timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
