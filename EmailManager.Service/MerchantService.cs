﻿using Crypteron.CipherObject;
using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Services.Exceptions;
using EmailManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class MerchantService : IMerchantService
    {
        private readonly EmailManagerDbContext context;

        public MerchantService(EmailManagerDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Merchant> AddMerchantAsync(string name, string merchantEmail)
        {
            var merchant = await this.context.Merchants.FirstOrDefaultAsync(m => m.PersonalEmail == merchantEmail);

            if (merchant != null)
            {
                throw new MerchantAlreadyExistsException($"Merchant with email address \"{merchantEmail}\" has been already added to the database!");
            }

            merchant = new Merchant()
            {
                Name = name,
                PersonalEmail = merchantEmail
            };

            merchant.Seal();

            await this.context.Merchants.AddAsync(merchant);
            await this.context.SaveChangesAsync();

            return merchant;
        }
    }
}
