﻿using EmailManager.Web.Models;

namespace EmailManager.Services.Contracts
{
    public interface IChangePasswordService
    {
        void ChangePasswordConfirm(ApplicationUser user);
    }
}


