﻿using EmailManager.Data.Models;
using EmailManager.Web.Models;
using System.Threading.Tasks;

namespace EmailManager.Services.Contracts
{
    public interface ILoanApplicationService
    {
        Task<LoanApplication> AddLoanApplicationAsync(LoanApplicant loanApplicant, int emailId);
    }
}
