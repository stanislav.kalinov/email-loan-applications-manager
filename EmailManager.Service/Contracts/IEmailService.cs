﻿using EmailManager.Data.Models;
using EmailManager.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailManager.Services.Contracts
{
    public interface IEmailService
    {
        Task<IList<ClientEmail>> ListAllEmails(int skip, int pageSize, string searchValue);
        Task<IList<ClientEmail>> ListNewEmails(int skip, int pageSize, string searchValue);
        Task<IList<ClientEmail>> ListOpenEmails(int skip, int pageSize, string searchValue);
        Task<IList<ClientEmail>> ListClosedEmails(int skip, int pageSize, string searchValue);
        int GetEmailsCount();
        int GetNewEmailsCount();
        int GetOpenEmailsCount();
        int GetClosedEmailsCount();
        ClientEmail GetEmail(int Id);
        Task<ClientEmail> ChangeStatusToNotReviewedAsync(int emailId, ApplicationUser user);
        Task<ClientEmail> ChangeStatusToNotValidAsync(int emailId, ApplicationUser user);
        Task<ClientEmail> ChangeStatusToNewAsync(int emailId, ApplicationUser user);
        Task<ClientEmail> ChangeStatusToOpenAsync(int emailId, ApplicationUser user);
        Task<ClientEmail> LoanApprovedAsync(int emailId, ApplicationUser user);
        Task<ClientEmail> LoanRejectedAsync(int emailId, ApplicationUser user);

    }
}
