﻿using System.Threading.Tasks;

namespace EmailManager.Services.Contracts
{
    public interface IGmailApiService
    {
        Task SaveEmailsToDbAsync(string accessToken);
    }
}
