﻿using EmailManager.Data.Models;
using System.Threading.Tasks;

namespace EmailManager.Services.Contracts
{
    public interface IEmailAttachmentService
    {
        Task<EmailAttachment> AddEmailAttachmentAsync(string fileName, int? size);
    }
}
