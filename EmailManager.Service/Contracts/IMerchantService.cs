﻿using EmailManager.Data.Models;
using System.Threading.Tasks;

namespace EmailManager.Services.Contracts
{
    public interface IMerchantService
    {
        Task<Merchant> AddMerchantAsync(string name, string merchantEmail);
    }
}
