﻿using EmailManager.Data.Models;
using System.Threading.Tasks;

namespace EmailManager.Services.Contracts
{
    public interface ILoanApplicantService
    {
        Task<LoanApplicant> AddLoanApplicantAsync(string name, string personalNumber, string phoneNumber);
    }
}
