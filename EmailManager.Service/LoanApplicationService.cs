﻿using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Services.Enums;
using EmailManager.Web.Data;
using EmailManager.Web.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class LoanApplicationService : ILoanApplicationService
    {
        private readonly EmailManagerDbContext context;

        public LoanApplicationService(EmailManagerDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<LoanApplication> AddLoanApplicationAsync(LoanApplicant loanApplicant, int emailId)
        {
            var email = await this.context.Emails.FirstOrDefaultAsync(e => e.Id == emailId);


            var loanApplication = new LoanApplication()
            {
                LoanApplicant = loanApplicant,
            };

            email.LoanApplication = loanApplication;

            var emailStatus = await this.context.EmailStatuses.FirstOrDefaultAsync(es => es.Type == ((EmailStatusType)4).ToString());

            if (emailStatus == null)
            {
                emailStatus = new EmailStatus()
                {
                    Type = ((EmailStatusType)4).ToString(),
                };

                await this.context.EmailStatuses.AddAsync(emailStatus);
            }

            email.EmailStatus = emailStatus;

            await this.context.LoanApplications.AddAsync(loanApplication);

            await this.context.SaveChangesAsync();

            return loanApplication;
        }
    }
}
