﻿using EmailManager.Data.Models;
using EmailManager.Services.Contracts;
using EmailManager.Services.Enums;
using EmailManager.Web.Data;
using EmailManager.Web.Models;
using log4net;
using log4net.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailManagerDbContext context;
        private readonly IMerchantService merchantService;
        private readonly IEmailAttachmentService emailAttachmentService;
        private readonly UserManager<ApplicationUser> _userManager;

        private static ILoggerRepository REPOSITORY = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
        private static readonly ILog log1 = LogManager.GetLogger(REPOSITORY.Name, "AuditLogLogger");

        public EmailService(
            UserManager<ApplicationUser> userManager,
            EmailManagerDbContext context, IMerchantService merchantService, IEmailAttachmentService emailAttachmentService)
        {
            this.context = context;
            this.merchantService = merchantService;
            this.emailAttachmentService = emailAttachmentService;
            _userManager = userManager;
        }

        public async Task<IList<ClientEmail>> ListAllEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
            else
            {
                var emails = await this.context.Emails
                    .Include(e => e.EmailBody)
                    .Include(e => e.EmailAttachments)
                    .Include(e => e.Merchant)
                    .Include(e => e.EmailStatus)
                    .OrderByDescending(a => a.ReceivedOn)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

                return emails;
            }
        }

        public async Task<IList<ClientEmail>> ListNewEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == ((EmailStatusType)3).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;

            }
            else
            {
                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == ((EmailStatusType)3).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
        }

        public async Task<IList<ClientEmail>> ListOpenEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == ((EmailStatusType)4).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;

            }
            else
            {
                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == ((EmailStatusType)4).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
        }

        public async Task<IList<ClientEmail>> ListClosedEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == ((EmailStatusType)5).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;

            }
            else
            {
                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == ((EmailStatusType)5).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Merchant)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ReceivedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
        }

        public int GetEmailsCount()
        {
            var count = this.context.Emails.Count();
            return count;
        }
        public int GetNewEmailsCount()
        {
            var count = this.context.Emails.Where(e => e.EmailStatus.Type == ((EmailStatusType)3).ToString()).Count();
            return count;
        }

        public int GetOpenEmailsCount()
        {
            var count = this.context.Emails.Where(e => e.EmailStatus.Type == ((EmailStatusType)4).ToString()).Count();
            return count;
        }

        public int GetClosedEmailsCount()
        {
            var count = this.context.Emails.Where(e => e.EmailStatus.Type == ((EmailStatusType)4).ToString()).Count();
            return count;
        }

        public ClientEmail GetEmail(int Id)
        {
            var email = this.context.Emails
                .Include(e => e.EmailBody)
                .Include(e => e.Merchant)
                .Include(e => e.EmailAttachments)
                .Include(e => e.EmailStatus)
                .Where(e => e.Id == Id).FirstOrDefault();
            return email;
        }

        public async Task<ClientEmail> ChangeStatusToNotReviewedAsync(int emailId, ApplicationUser user)
        {
            var email = await this.context.Emails.Include(e => e.EmailStatus).FirstOrDefaultAsync(e => e.Id == emailId);

            var oldStatus = email.EmailStatus.Type;

            var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.Type == ((EmailStatusType)1).ToString());

            if (newStatus == null)
            {
                newStatus = new EmailStatus() { Type = ((EmailStatusType)1).ToString() };
                await this.context.EmailStatuses.AddAsync(newStatus);
            }

            email.EmailStatus = newStatus;
            email.CurrentStatusSetOn = DateTime.Now;
            email.User = user;

            await this.context.SaveChangesAsync();

            log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", Action = $"{"Status changed"}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });
            //log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });

            log1.Info("????????????????????????????????????????????????????????????");
            log1.Info($"EmailId={email.Id}; Timestamp={DateTime.Now}; Action={"Status changed"}; User={user.UserName}; OldStatus={oldStatus}; NewStatus={newStatus}");


            return email;
        }

        public async Task<ClientEmail> ChangeStatusToNotValidAsync(int emailId, ApplicationUser user)
        {
            var email = await this.context.Emails.Include(e => e.EmailStatus).FirstOrDefaultAsync(e => e.Id == emailId);

            var oldStatus = email.EmailStatus.Type;

            var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.Type == ((EmailStatusType)2).ToString());

            if (newStatus == null)
            {
                newStatus = new EmailStatus() { Type = ((EmailStatusType)2).ToString() };
                await this.context.EmailStatuses.AddAsync(newStatus);
            }

            email.EmailStatus = newStatus;
            email.CurrentStatusSetOn = DateTime.Now;
            email.User = user;

            await this.context.SaveChangesAsync();

            log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", Action = $"{"Status changed"}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });
            //log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });


            log1.Info("????????????????????????????????????????????????????????????");
            log1.Info($"EmailId={email.Id}; Timestamp={DateTime.Now}; Action={"Status changed"}; User={user.UserName}; OldStatus={oldStatus}; NewStatus={newStatus}");

            return email;
        }

        public async Task<ClientEmail> ChangeStatusToNewAsync(int emailId, ApplicationUser user)
        {
            var email = await this.context.Emails.Include(e => e.EmailStatus).FirstOrDefaultAsync(e => e.Id == emailId);

            var oldStatus = email.EmailStatus.Type;

            var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.Type == ((EmailStatusType)3).ToString());

            if (newStatus == null)
            {
                newStatus = new EmailStatus() { Type = ((EmailStatusType)3).ToString() };
                await this.context.EmailStatuses.AddAsync(newStatus);
            }

            email.EmailStatus = newStatus;
            email.CurrentStatusSetOn = DateTime.Now;
            email.User = user;

            await this.context.SaveChangesAsync();

            log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", Action = $"{"Status changed"}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });
            //log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });


            log1.Info("????????????????????????????????????????????????????????????");
            log1.Info($"EmailId={email.Id}; Timestamp={DateTime.Now}; Action={"Status changed"}; User={user.UserName}; OldStatus={oldStatus}; NewStatus={newStatus}");

            return email;
        }

        public async Task<ClientEmail> ChangeStatusToOpenAsync(int emailId, ApplicationUser user)
        {
            var email = await this.context.Emails.Include(e => e.EmailStatus).FirstOrDefaultAsync(e => e.Id == emailId);

            var oldStatus = email.EmailStatus.Type;

            var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.Type == ((EmailStatusType)4).ToString());

            if (newStatus == null)
            {
                newStatus = new EmailStatus() { Type = ((EmailStatusType)4).ToString() };
                await this.context.EmailStatuses.AddAsync(newStatus);

            }

            email.EmailStatus = newStatus;
            email.CurrentStatusSetOn = DateTime.Now;
            email.User = user;

            await this.context.SaveChangesAsync();

            log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", Action = $"{"Status changed"}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });
            //log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });


            log1.Info($"EmailId={email.Id}; Timestamp={DateTime.Now}; Action={"Status changed"}; User={user.UserName}; OldStatus={oldStatus}; NewStatus={newStatus}");

            return email;
        }

        public async Task<ClientEmail> LoanApprovedAsync(int emailId, ApplicationUser user)
        {
            var email = await this.context.Emails.Include(e => e.EmailStatus)
                .Include(e => e.LoanApplication)
                .ThenInclude(e => e.LoanDecision)
                .FirstOrDefaultAsync(e => e.Id == emailId);

            var oldStatus = email.EmailStatus.Type;

            var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.Type == ((EmailStatusType)5).ToString());

            if (newStatus == null)
            {
                newStatus = new EmailStatus() { Type = ((EmailStatusType)5).ToString() };
                await this.context.EmailStatuses.AddAsync(newStatus);
            }

            email.EmailStatus = newStatus;

            var newDecision = this.context.LoanDecisions.FirstOrDefault(es => es.BankDecision == ((LoanApplicationDecision)1).ToString());

            if (newDecision == null)
            {
                newDecision = new LoanDecision() { BankDecision = ((LoanApplicationDecision)1).ToString() };
                await this.context.LoanDecisions.AddAsync(newDecision);
            }

            email.LoanApplication.LoanDecision= newDecision;
            email.LoanApplication.User = user;
            email.LoanApplication.LoanDecisionSetOn = DateTime.Now;

            email.CurrentStatusSetOn = DateTime.Now;
            email.User = user;

            await this.context.SaveChangesAsync();

            log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", Action = $"{"Status changed"}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });
            //log1.Info(new { EmailId = $"{email.Id}", Timestamp = $"{ DateTime.Now}", OldStatus = $"{ oldStatus}", NewStatus = $"{ newStatus}" });


            log1.Info($"EmailId={email.Id}; Timestamp={DateTime.Now}; Action={"Status changed"}; User={user.UserName}; OldStatus={oldStatus}; NewStatus={newStatus}");

            return email;
        }

        public async Task<ClientEmail> LoanRejectedAsync(int emailId, ApplicationUser user)
        {
            var email = await this.context.Emails.Include(e => e.EmailStatus)
              .Include(e => e.LoanApplication)
              .ThenInclude(e => e.LoanDecision)
              .FirstOrDefaultAsync(e => e.Id == emailId);

            var oldStatus = email.EmailStatus.Type;

            var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.Type == ((EmailStatusType)5).ToString());

            if (newStatus == null)
            {
                newStatus = new EmailStatus() { Type = ((EmailStatusType)5).ToString() };
                await this.context.EmailStatuses.AddAsync(newStatus);
            }

            email.EmailStatus = newStatus;

            var newDecision = this.context.LoanDecisions.FirstOrDefault(es => es.BankDecision == ((LoanApplicationDecision)2).ToString());

            if (newDecision == null)
            {
                newDecision = new LoanDecision() { BankDecision = ((LoanApplicationDecision)2).ToString() };
                await this.context.LoanDecisions.AddAsync(newDecision);
            }

            email.LoanApplication.LoanDecision = newDecision;
            email.LoanApplication.User = user;
            email.LoanApplication.LoanDecisionSetOn = DateTime.Now;

            email.CurrentStatusSetOn = DateTime.Now;
            email.User = user;

            await this.context.SaveChangesAsync();

            log1.Info($"EmailId={email.Id}; Timestamp={DateTime.Now}; Action={"Status changed"}; User={user.UserName}; OldStatus={oldStatus}; NewStatus={newStatus}");

            return email;
        }

    }
}
