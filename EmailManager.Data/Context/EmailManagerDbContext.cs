﻿using EmailManager.Data.Configurations;
using EmailManager.Data.Models;
using EmailManager.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace EmailManager.Web.Data
{
    public class EmailManagerDbContext : IdentityDbContext<ApplicationUser>
    {
        public EmailManagerDbContext(DbContextOptions<EmailManagerDbContext> options)
            : base(options)
        {
        }
        public EmailManagerDbContext()
        {
        }

        public DbSet<ClientEmail> Emails { get; set; }
        public DbSet<EmailAttachment> EmailAttachments { get; set; }
        public DbSet<EmailBody> EmailBodies { get; set; }
        public DbSet<ApplicationUser> EmailManagerUsers { get; set; }
        public DbSet<LoanApplicant> LoanApplicants { get; set; }
        public DbSet<LoanApplication> LoanApplications { get; set; }
        public DbSet<LoanDecision> LoanDecisions { get; set; }
        public DbSet<EmailStatus> EmailStatuses { get; set; }
        public DbSet<Merchant> Merchants { get; set; }
        public DbSet<GmailLoginCredential> GmailLoginCredentials { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //this.SeedJSONData(modelBuilder);

            builder.ApplyConfiguration(new EmailAttachmentConfiguration());
            builder.ApplyConfiguration(new EmailBodyConfiguration());
            builder.ApplyConfiguration(new ClientEmailConfiguration());
            builder.ApplyConfiguration(new LoanApplicantConfiguration());
            builder.ApplyConfiguration(new LoanApplicationConfiguration());
            builder.ApplyConfiguration(new LoanDecisionConfiguration());
            builder.ApplyConfiguration(new EmailStatusConfiguration());
            builder.ApplyConfiguration(new MerchantConfiguration());

            base.OnModelCreating(builder);
        }

        //private void SeedJSONData(ModelBuilder modelBuilder)
        //{
        //    var movies = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Movies.json");
        //    var directors = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Directors.json");
        //    var actors = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Actors.json");
        //    var genres = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Genres.json");
        //    var previews = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Previews.json");
        //    var moviesActors = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\MoviesActors.json");
        //    var moviesGenres = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\MoviesGenres.json");
        //    var reviews = File.ReadAllText(@"..\\IMovieDb.Data\JSON.Data\Reviews.json");

        //    var jsonMovies = JsonConvert.DeserializeObject<Movie[]>(movies);
        //    var jsonDirectors = JsonConvert.DeserializeObject<Director[]>(directors);
        //    var jsonActors = JsonConvert.DeserializeObject<Actor[]>(actors);
        //    var jsonGenres = JsonConvert.DeserializeObject<Genre[]>(genres);
        //    var jsonPreviews = JsonConvert.DeserializeObject<Preview[]>(previews);
        //    var jsonMoviesActors = JsonConvert.DeserializeObject<MoviesActors[]>(moviesActors);
        //    var jsonMoviesGenres = JsonConvert.DeserializeObject<MoviesGenres[]>(moviesGenres);
        //    var jsonReviews = JsonConvert.DeserializeObject<Review[]>(reviews);


        //    modelBuilder.Entity<Director>().HasData(jsonDirectors);
        //    modelBuilder.Entity<Actor>().HasData(jsonActors);
        //    modelBuilder.Entity<Genre>().HasData(jsonGenres);
        //    modelBuilder.Entity<Preview>().HasData(jsonPreviews);
        //    modelBuilder.Entity<Movie>().HasData(jsonMovies);
        //    modelBuilder.Entity<MoviesActors>().HasData(jsonMoviesActors);
        //    modelBuilder.Entity<MoviesGenres>().HasData(jsonMoviesGenres);
        //    modelBuilder.Entity<Review>().HasData(jsonReviews);
        //}

        public static void SeedUserAdmin(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<EmailManagerDbContext>();

                if (dbContext.Roles.Any(u => u.Name == "Manager"))
                {
                    return;
                }

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                roleManager.CreateAsync(new IdentityRole { Name = "Manager" }).Wait();
                roleManager.CreateAsync(new IdentityRole { Name = "Operator" }).Wait();

                var adminUser = new ApplicationUser { UserName = "manager@manager.com", Email = "manager@manager.com" };
                userManager.CreateAsync(adminUser, "TBIManager2019@").Wait();

                userManager.AddToRoleAsync(adminUser, "Manager").Wait();
            }
        }
    }
}
