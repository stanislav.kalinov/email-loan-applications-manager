﻿using EmailManager.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailManager.Data.Configurations
{
    public class EmailAttachmentConfiguration : IEntityTypeConfiguration<EmailAttachment>
    {
        public void Configure(EntityTypeBuilder<EmailAttachment> builder)
        {
            builder.HasKey(ea => ea.Id);

            builder.Property(ea => ea.Size)
              .IsRequired();

            builder.Property(ea => ea.FileName)
             .IsRequired();

            builder
                .HasOne(ea => ea.Email)
                .WithMany(e => e.EmailAttachments)
                .HasForeignKey(ea => ea.EmailId);
        }
    }
}
