﻿using EmailManager.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailManager.Data.Configurations
{
    public class LoanDecisionConfiguration : IEntityTypeConfiguration<LoanDecision>
    {
        public void Configure(EntityTypeBuilder<LoanDecision> builder)
        {
            builder.HasKey(ld => ld.Id);

            builder.Property(ld => ld.BankDecision)
                .IsRequired();
        }
    }
}
