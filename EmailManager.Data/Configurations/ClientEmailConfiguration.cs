﻿using EmailManager.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailManager.Data.Configurations
{
    public class ClientEmailConfiguration : IEntityTypeConfiguration<ClientEmail>
    {
        public void Configure(EntityTypeBuilder<ClientEmail> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Subject)
               .HasMaxLength(50);

            builder.Property(e => e.ReceivedOn)
                .IsRequired();

            builder
              .HasOne(e => e.EmailBody)
              .WithOne(eb => eb.ClientEmail)
              .HasForeignKey<ClientEmail>(e => e.BodyId);

            builder
              .HasOne(e => e.EmailStatus)
              .WithMany(es => es.ClientEmails)
              .HasForeignKey(e => e.StatusId);

            builder
              .HasOne(e => e.Merchant)
              .WithMany(m => m.Emails)
              .HasForeignKey(e => e.MerchantId);
        }
    }
}