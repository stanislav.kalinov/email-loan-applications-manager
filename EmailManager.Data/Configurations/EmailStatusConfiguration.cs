﻿using EmailManager.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailManager.Data.Configurations
{
    public class EmailStatusConfiguration : IEntityTypeConfiguration<EmailStatus>
    {
        public void Configure(EntityTypeBuilder<EmailStatus> builder)
        {
            builder.HasKey(ms => ms.Id);

            builder.Property(md => md.Type)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}
