﻿using EmailManager.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailManager.Data.Configurations
{
    public class LoanApplicationConfiguration : IEntityTypeConfiguration<LoanApplication>
    {

        public void Configure(EntityTypeBuilder<LoanApplication> builder)
        {
            builder.HasKey(la => la.Id);

            builder
               .HasOne(la => la.LoanApplicant)
               .WithMany(a => a.LoanApplications)
               .HasForeignKey(la => la.ApplicantId);

            builder
               .HasOne(la => la.LoanDecision)
               .WithMany(ld => ld.LoanApplications)
               .HasForeignKey(la => la.DecisionId);
        }
    }
}
