﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailManager.Data.Migrations
{
    public partial class fdfgd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Emails_AspNetUsers_CurrentStatusSetByUserId",
                table: "Emails");

            migrationBuilder.DropForeignKey(
                name: "FK_LoanApplications_AspNetUsers_CurrentStatusSetByUserId",
                table: "LoanApplications");

            migrationBuilder.DropForeignKey(
                name: "FK_LoanApplications_LoanStatuses_StatusId",
                table: "LoanApplications");

            migrationBuilder.DropIndex(
                name: "IX_LoanApplications_CurrentStatusSetByUserId",
                table: "LoanApplications");

            migrationBuilder.DropIndex(
                name: "IX_LoanApplications_StatusId",
                table: "LoanApplications");

            migrationBuilder.DropIndex(
                name: "IX_Emails_CurrentStatusSetByUserId",
                table: "Emails");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "LoanApplications");

            migrationBuilder.DropColumn(
                name: "TerminatedOn",
                table: "LoanApplications");

            migrationBuilder.DropColumn(
                name: "EmailToWorkId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LoanApplicationId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "CurrentStatusSetOn",
                table: "LoanApplications",
                newName: "LoanDecisionSetOn");

            migrationBuilder.RenameColumn(
                name: "CurrentStatusSetByUserId",
                table: "LoanApplications",
                newName: "UserId");

            migrationBuilder.AddColumn<int>(
                name: "LoanApplicationId",
                table: "LoanStatuses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LoanDecisionSetByUserId",
                table: "LoanApplications",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CurrentStatusSetOn",
                table: "Emails",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Emails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LoanStatuses_LoanApplicationId",
                table: "LoanStatuses",
                column: "LoanApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanApplications_UserId",
                table: "LoanApplications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Emails_UserId",
                table: "Emails",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Emails_AspNetUsers_UserId",
                table: "Emails",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LoanApplications_AspNetUsers_UserId",
                table: "LoanApplications",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LoanStatuses_LoanApplications_LoanApplicationId",
                table: "LoanStatuses",
                column: "LoanApplicationId",
                principalTable: "LoanApplications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Emails_AspNetUsers_UserId",
                table: "Emails");

            migrationBuilder.DropForeignKey(
                name: "FK_LoanApplications_AspNetUsers_UserId",
                table: "LoanApplications");

            migrationBuilder.DropForeignKey(
                name: "FK_LoanStatuses_LoanApplications_LoanApplicationId",
                table: "LoanStatuses");

            migrationBuilder.DropIndex(
                name: "IX_LoanStatuses_LoanApplicationId",
                table: "LoanStatuses");

            migrationBuilder.DropIndex(
                name: "IX_LoanApplications_UserId",
                table: "LoanApplications");

            migrationBuilder.DropIndex(
                name: "IX_Emails_UserId",
                table: "Emails");

            migrationBuilder.DropColumn(
                name: "LoanApplicationId",
                table: "LoanStatuses");

            migrationBuilder.DropColumn(
                name: "LoanDecisionSetByUserId",
                table: "LoanApplications");

            migrationBuilder.DropColumn(
                name: "CurrentStatusSetOn",
                table: "Emails");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Emails");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "LoanApplications",
                newName: "CurrentStatusSetByUserId");

            migrationBuilder.RenameColumn(
                name: "LoanDecisionSetOn",
                table: "LoanApplications",
                newName: "CurrentStatusSetOn");

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "LoanApplications",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TerminatedOn",
                table: "LoanApplications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmailToWorkId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoanApplicationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LoanApplications_CurrentStatusSetByUserId",
                table: "LoanApplications",
                column: "CurrentStatusSetByUserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LoanApplications_StatusId",
                table: "LoanApplications",
                column: "StatusId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Emails_CurrentStatusSetByUserId",
                table: "Emails",
                column: "CurrentStatusSetByUserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Emails_AspNetUsers_CurrentStatusSetByUserId",
                table: "Emails",
                column: "CurrentStatusSetByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LoanApplications_AspNetUsers_CurrentStatusSetByUserId",
                table: "LoanApplications",
                column: "CurrentStatusSetByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LoanApplications_LoanStatuses_StatusId",
                table: "LoanApplications",
                column: "StatusId",
                principalTable: "LoanStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
