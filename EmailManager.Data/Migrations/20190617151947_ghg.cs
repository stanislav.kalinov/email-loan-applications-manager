﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EmailManager.Data.Migrations
{
    public partial class ghg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoanStatuses");

            migrationBuilder.DropColumn(
                name: "LoanDecisionSetByUserId",
                table: "LoanApplications");

            migrationBuilder.DropColumn(
                name: "CurrentStatusSetByUserId",
                table: "Emails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LoanDecisionSetByUserId",
                table: "LoanApplications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentStatusSetByUserId",
                table: "Emails",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LoanStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    LoanApplicationId = table.Column<int>(nullable: true),
                    Type = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanStatuses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoanStatuses_LoanApplications_LoanApplicationId",
                        column: x => x.LoanApplicationId,
                        principalTable: "LoanApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoanStatuses_LoanApplicationId",
                table: "LoanStatuses",
                column: "LoanApplicationId");
        }
    }
}
