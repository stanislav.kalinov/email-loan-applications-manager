﻿using Crypteron;
using System.Collections.Generic;

namespace EmailManager.Data.Models
{
    public class Merchant
    {
        public Merchant()
        {
            this.Emails = new HashSet<ClientEmail>();
        }

        public int Id { get; set; }

        [Secure]
        public string Name { get; set; }

        public string PersonalEmail { get; set; }

        public ICollection<ClientEmail> Emails { get; set; }
    }
}
