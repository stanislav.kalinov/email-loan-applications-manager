﻿using EmailManager.Web.Models;
using System;

namespace EmailManager.Data.Models
{
    public class LoanApplication
    {
        public int Id { get; set; }
        public DateTime? LoanDecisionSetOn { get; set; }

        public ApplicationUser User { get; set; }

        public int? ApplicantId { get; set; }
        public LoanApplicant LoanApplicant { get; set; }

        public int? DecisionId { get; set; }
        public LoanDecision LoanDecision { get; set; }
    }
}
