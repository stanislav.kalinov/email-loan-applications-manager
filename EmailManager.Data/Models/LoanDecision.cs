﻿using System.Collections.Generic;

namespace EmailManager.Data.Models
{
    public class LoanDecision
    {
        public LoanDecision()
        {
            this.LoanApplications = new HashSet<LoanApplication>();
        }

        public int Id { get; set; }
        public string BankDecision { get; set; }

        public ICollection<LoanApplication> LoanApplications { get; set; }
    }
}
