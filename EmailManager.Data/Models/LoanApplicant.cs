﻿using Crypteron;
using System.Collections.Generic;

namespace EmailManager.Data.Models
{
    public class LoanApplicant
    {
        public LoanApplicant()
        {
            this.LoanApplications = new HashSet<LoanApplication>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        [Secure]
        public string PersonalNumber { get; set; }
        public string PhoneNumber { get; set; }

        public ICollection<LoanApplication> LoanApplications { get; set; }
    }
}
