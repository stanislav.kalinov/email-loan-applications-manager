﻿using System;

namespace EmailManager.Data.Models
{
    public class GmailLoginCredential
    {
        public int Id { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
