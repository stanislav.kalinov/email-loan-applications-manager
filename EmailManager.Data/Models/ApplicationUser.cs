﻿using Microsoft.AspNetCore.Identity;

namespace EmailManager.Web.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool ChangePassword { get; set; }
    }
}
