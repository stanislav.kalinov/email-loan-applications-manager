﻿using EmailManager.Web.Models;
using System;
using System.Collections.Generic;

namespace EmailManager.Data.Models
{
    public class ClientEmail
    {
        public ClientEmail()
        {
            this.EmailAttachments = new HashSet<EmailAttachment>();
        }

        public int Id { get; set; }
        public string MailboxEmailId { get; set; }
        public string Subject { get; set; }
        public long Timestamp { get; set; }
        public DateTime ReceivedOn { get; set; }
        public DateTime CurrentStatusSetOn { get; set; }

        public int? BodyId { get; set; }
        public EmailBody EmailBody { get; set; }

        public int? LoanApplicationId { get; set; }
        public LoanApplication LoanApplication { get; set; }

        public int? MerchantId { get; set; }
        public Merchant Merchant { get; set; }

        public int? StatusId { get; set; }
        public EmailStatus EmailStatus { get; set; }

        public ApplicationUser User { get; set; }

        public ICollection<EmailAttachment> EmailAttachments { get; set; }
    }
}
