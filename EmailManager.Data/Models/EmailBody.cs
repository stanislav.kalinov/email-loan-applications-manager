﻿using Crypteron;

namespace EmailManager.Data.Models
{
    public class EmailBody
    {
        public int Id { get; set; }

        [Secure]
        public string Content { get; set; }

        public ClientEmail ClientEmail { get; set; }
    }
}
