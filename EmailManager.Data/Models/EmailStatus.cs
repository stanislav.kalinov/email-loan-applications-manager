﻿using System.Collections.Generic;

namespace EmailManager.Data.Models
{
    public class EmailStatus
    {
        public EmailStatus()
        {
            this.ClientEmails = new HashSet<ClientEmail>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public ICollection<ClientEmail> ClientEmails { get; set; }
    }
}
