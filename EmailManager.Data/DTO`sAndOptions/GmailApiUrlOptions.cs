﻿namespace EmailManager.Data.DTO_sAndOptions
{
    public class GmailApiUrlOptions
    {
        public string Base { get; set; }
        public string Messages { get; set; }
        public string Message { get; set; }
        public string InboxMessages { get; set; }
    }
}
