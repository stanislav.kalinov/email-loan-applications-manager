﻿using Newtonsoft.Json;

namespace EmailManager.Data.DTO_sAndOptions
{
    public class GmailLoginCredentials
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresInSec { get; set; }
    }
}
