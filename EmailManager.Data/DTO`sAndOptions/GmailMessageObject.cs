﻿using EmailManager.Data.DTO_sAndOptions;
using Newtonsoft.Json;

namespace EmailManager.Services
{
    public class GmailMessageObject
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("threadId")]
        public string ThreadId { get; set; }

        [JsonProperty("internalDate")]
        public long InternalDate { get; set; }

        [JsonProperty("payload")]
        public GmailMessagePayload Payload { get; set; }
    }
}
