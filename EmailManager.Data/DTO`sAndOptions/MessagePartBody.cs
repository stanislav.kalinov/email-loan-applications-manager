﻿using Newtonsoft.Json;

namespace EmailManager.Data.DTO_sAndOptions
{
    public class MessagePartBody
    {
        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }
    }
}
