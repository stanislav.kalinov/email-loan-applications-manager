﻿using Newtonsoft.Json;

namespace EmailManager.Services
{
    public class GoogleAccessToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("expires_in")]
        public int ExpireIn { get; set; }
    }
}
