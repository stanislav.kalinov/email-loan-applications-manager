﻿using Newtonsoft.Json;

namespace EmailManager.Data.DTO_sAndOptions
{
    public class ModifyMessageRequestContent
    {
        [JsonProperty("addLabelIds")]
        public string[] AddLabels { get; set; }

        [JsonProperty("removeLabelIds")]
        public string[] RemoveLabels { get; set; }
    }
}
