﻿using Newtonsoft.Json;

namespace EmailManager.Data.DTO_sAndOptions
{
    public class MessagePartHeader
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
