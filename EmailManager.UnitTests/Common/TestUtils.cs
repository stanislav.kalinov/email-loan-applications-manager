﻿using EmailManager.Web.Data;
using Microsoft.EntityFrameworkCore;

namespace EmailManager.UnitTests.Common
{
    public static class TestUtils
    {
        public static DbContextOptions<EmailManagerDbContext> GetOptions(string databaseName)
        {
            //var provider = new ServiceCollection().AddEntityFrameworkInMemoryDatabase().BuildServiceProvider();

            //return new DbContextOptionsBuilder().UseInMemoryDatabase(databaseName).UseInternalServiceProvider(provider).Options;

            return new DbContextOptionsBuilder<EmailManagerDbContext>().UseInMemoryDatabase(databaseName).Options;
        }
    }
}
